﻿namespace ContactApi.Models
{
    public class GeneralOutputModel
    {
        public string Status { get; set; }
        public object Result { get; set; }
        public string Message { get; set; }
    }
}

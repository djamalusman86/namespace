﻿namespace ContactApi.Models
{
    public class AddContactRequest
    {
        public Guid? Idcntc { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public Guid idPhone { get; set; }
        public Guid idAddres { get; set; }

        public Guid idGroup { get; set; }
    }
}

﻿using System.Text.RegularExpressions;

namespace ContactApi.Models
{
    public class ContactGroupModel
    {
        public Guid? ContactId { get; set; }
        public Guid? GroupId { get; set; }
    }
}

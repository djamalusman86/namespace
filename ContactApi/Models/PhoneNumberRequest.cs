﻿namespace ContactApi.Models
{
    public class PhoneNumberRequest
    {
        public Guid? id { get; set; }
        public string PhoneNumber { get; set; }
    }
}

﻿namespace ContactApi.Models
{
    public class AddressModel
    {
        public Guid IdAdress { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public Guid? ContactId { get; set; }
    }
}

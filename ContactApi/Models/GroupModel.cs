﻿namespace ContactApi.Models
{
    public class GroupModel
    {
        public Guid? IdGroup { get; set; }
        public string Name { get; set; }
    }
}

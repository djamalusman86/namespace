﻿namespace ContactApi.Models
{
    public class UpdateContact
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public Guid IdPhone { get; set; }
        public Guid IdAddres { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace ContactApi.Models
{
    public class PhoneNumberModel
    {
        [Key]
        public Guid? IdPhone { get; set; }
        public string Number { get; set; }
        public Guid? ContactId { get; set; }
    }
}

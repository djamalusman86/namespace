﻿namespace ContactApi.Models
{
    public class GroupRequest
    {
        public Guid? idgrp { get; set; }
        public string GroupName { get; set; }
    }
}

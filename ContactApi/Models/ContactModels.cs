﻿namespace ContactApi.Models
{
    public class ContactModels
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
    }
}

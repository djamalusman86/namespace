﻿namespace ContactApi.Models
{
    public class AdressRequest
    {
        public Guid? idadres { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zipCode { get; set; }

    }
}

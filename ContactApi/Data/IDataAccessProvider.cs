﻿using ContactApi.Models;

namespace ContactApi.Data
{
    public interface IDataAccessProvider
    {
        GeneralOutputModel GetDetailContacts();
        GeneralOutputModel GetDetailGroup();
        GeneralOutputModel GetDetailAdress();
        GeneralOutputModel GetDetailPhoneNumber();
        
        GeneralOutputModel AddMasterPhoneNumber(PhoneNumberRequest pr);
        GeneralOutputModel AddMasterAdress(AdressRequest pr);
        GeneralOutputModel AddMasterGroup(GroupRequest pr);
        GeneralOutputModel AddContact(AddContactRequest pr);
        GeneralOutputModel UpdateMasterPhoneNumber(PhoneNumberRequest pr);
        GeneralOutputModel UpdateMasterAdress(AdressRequest pr);
        GeneralOutputModel UpdateMasterGroup(GroupRequest pr);
        GeneralOutputModel UpdateContact(AddContactRequest pr);

        GeneralOutputModel DeleteMasterPhoneNumber(PhoneNumberRequest pr);
        GeneralOutputModel DeleteMasterAdress(AdressRequest pr);
        GeneralOutputModel DeleteMasterGroup(GroupRequest pr);
        GeneralOutputModel DeleteContact(AddContactRequest pr);

    }
}

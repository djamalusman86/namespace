﻿using Microsoft.EntityFrameworkCore;
using ContactApi.Models;
using System.Net;
namespace ContactApi.Data
{
    public class ContactAPiDbContext : DbContext
    {
        public ContactAPiDbContext (DbContextOptions options) : base(options) { }

        public DbSet<ContactModels> Contacts  {  get; set; }
        public DbSet<PhoneNumberModel> PhoneNumber {  get; set; }
        public DbSet<AddressModel> Address {  get; set; }
        public DbSet<GroupModel> GroupModel {  get; set; }
        public DbSet<ContactGroupModel> ContactGroup {  get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AddressModel>()
                .HasNoKey();
            modelBuilder.Entity<ContactGroupModel>()
                .HasNoKey();
            modelBuilder.Entity<GroupModel>()
                .HasNoKey();
            modelBuilder.Entity<PhoneNumberModel>()
                .HasNoKey();
            modelBuilder.Entity<ContactModels>()
                .HasNoKey();
            modelBuilder.Entity<PhoneNumberModel>()
                .HasKey(p => p.IdPhone);
            modelBuilder.Entity<AddressModel>()
                .HasKey(p => p.IdAdress);
            modelBuilder.Entity<GroupModel>()
                .HasKey(p => p.IdGroup);
            modelBuilder.Entity<ContactModels>()
                .HasKey(p => p.Id);
        }
    }
}

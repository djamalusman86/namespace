﻿using ContactApi.Models;

namespace ContactApi.Data
{
    public class DataAccessProvider : IDataAccessProvider
    {
        private readonly ContactAPiDbContext db;
        GeneralOutputModel output = new GeneralOutputModel();


        public DataAccessProvider(ContactAPiDbContext _db)
        {
            db = _db;
        }
        
        public GeneralOutputModel AddMasterPhoneNumber(PhoneNumberRequest pr)
        {
            try
            {

                PhoneNumberModel b = new PhoneNumberModel();
                if (pr.PhoneNumber != null)
                {


                    b.Number = pr.PhoneNumber;
                    db.PhoneNumber.Add(b);
                    db.SaveChanges();


                    output.Status = "OK";
                    output.Result = b.Number;
                    output.Message = "Insert Success";
                }

                else
                {
                    output.Status = "NG";
                    output.Message = "Failed Insert";
                }

            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = "Failed Insert";
            }
            return output;
        }

        public GeneralOutputModel AddMasterAdress(AdressRequest pr)
        {
            try
            {
                
                AddressModel c = new AddressModel();
                if (pr.city != null)
                {


                    c.ZipCode = pr.zipCode;
                    c.State = pr.state;
                    c.Street = pr.street;
                    c.City = pr.city;
                    db.Address.Add(c);
                    db.SaveChanges();


                    output.Status = "OK";
                    output.Result = c.ZipCode;
                    output.Message = "Insert Success";
                }

                else
                {
                    output.Status = "NG";
                    output.Message = "Failed Insert";
                }

            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = "Failed Insert";
            }
            return output;
        }

        public GeneralOutputModel AddMasterGroup(GroupRequest pr)
        {
            try
            {
                GroupModel e = new GroupModel();
                if (pr.GroupName != null)
                {
                    e.Name = pr.GroupName;
                    db.GroupModel.Add(e);
                    db.SaveChanges();


                    output.Status = "OK";
                    output.Result = e.Name;
                    output.Message = "Insert Success";
                }

                else
                {
                    output.Status = "NG";
                    output.Message = "Failed Insert";
                }


            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = "Failed Insert";
            }
            return output;
        }

        public GeneralOutputModel AddContact(AddContactRequest pr)
        {
            try
            {
                ContactModels a = new ContactModels();
                PhoneNumberModel b = new PhoneNumberModel();
                AddressModel c = new AddressModel();
                ContactGroupModel D = new ContactGroupModel();
                if (pr.FullName != null && pr.Email != null)
                {
                    a.FullName = pr.FullName;
                    a.Email = pr.Email;

                    db.Contacts.Add(a);
                    db.SaveChanges();

                    var newId = a.Id;

                    b.ContactId = newId;
                    db.PhoneNumber.Add(b);
                    db.SaveChanges();

                    c.ContactId = newId;
                    db.Address.Add(c);
                    db.SaveChanges();

                    D.ContactId = newId;
                    D.GroupId = pr.idGroup;
                    db.ContactGroup.Add(D);
                    db.SaveChanges();


                    output.Status = "OK";
                    output.Result = a.Id;
                    output.Message = "Insert Success";
                }

                else
                {
                    output.Status = "NG";
                    output.Message = "Failed Insert";
                }


            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = "Failed Insert";
            }
            return output;
        }



        public GeneralOutputModel UpdateMasterPhoneNumber(PhoneNumberRequest pr)
        {
            try
            {
                var dt = db.PhoneNumber.Where(p => p.IdPhone == pr.id).FirstOrDefault();
                if (dt == null)
                {
                    output.Status = "NG";
                    output.Message = "update Failed!";

                    return output;
                }
                dt.Number = pr.PhoneNumber;
                db.SaveChanges();


                output.Status = "OK";
                output.Result = dt.Number;
                output.Message = "update Success";
                return output;
            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = "Failed Insert";
            }
            return output;
        }

        public GeneralOutputModel UpdateMasterAdress(AdressRequest pr)
        {
            try
            {
                var dt = db.Address.Where(p => p.IdAdress == pr.idadres).FirstOrDefault();
                if (dt == null)
                {
                    output.Status = "NG";
                    output.Message = "update Failed!";

                    return output;
                }
                dt.ZipCode = pr.zipCode;
                dt.State = pr.state;
                dt.Street = pr.street;
                dt.City = pr.city;
                db.SaveChanges();


                output.Status = "OK";
                output.Result = dt.City;
                output.Message = "update Success";
                return output;
            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = "Failed Insert";
            }
            return output;
        }

        public GeneralOutputModel UpdateMasterGroup(GroupRequest pr)
        {
            try
            {
                var dt = db.GroupModel.Where(p => p.IdGroup == pr.idgrp).FirstOrDefault();
                if (dt == null)
                {
                    output.Status = "NG";
                    output.Message = "update Failed!";

                    return output;
                }
                dt.Name = pr.GroupName;
                db.SaveChanges();


                output.Status = "OK";
                output.Result = dt.Name;
                output.Message = "update Success";
                return output;
            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = "Failed Insert";
            }
            return output;
        }

        public GeneralOutputModel UpdateContact(AddContactRequest pr)
        {
            try
            {
                var dt = db.Contacts.Where(p => p.Id == pr.Idcntc).FirstOrDefault();
                if (dt == null)
                {
                    output.Status = "NG";
                    output.Message = "update Failed!";

                    return output;
                }
                dt.FullName = pr.FullName;
                dt.Email = pr.Email;
                db.SaveChanges();


                output.Status = "OK";
                output.Result = dt.FullName;
                output.Message = "update Success";
                return output;
            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = "Failed Insert";
            }
            return output;
        }


        public GeneralOutputModel DeleteMasterPhoneNumber(PhoneNumberRequest pr)
        {
            try
            {
                var dt = db.PhoneNumber.Where(p => p.IdPhone == pr.id).FirstOrDefault();
                if (dt == null)
                {
                    output.Status = "NG";
                    output.Message = "Hapus Failed!";

                    return output;
                }
                else
                {
                     db.PhoneNumber.Remove(dt);
                     db.SaveChanges();


                    output.Status = "OK";
                    output.Result = dt.Number;
                    output.Message = "Hapus Success";
                }
                
                return output;
            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = "Failed Insert";
            }
            return output;
        }

        public GeneralOutputModel DeleteMasterAdress(AdressRequest pr)
        {
            try
            {
                var dt = db.Address.Where(p => p.IdAdress == pr.idadres).FirstOrDefault();
                if (dt == null)
                {
                    output.Status = "NG";
                    output.Message = "update Failed!";

                    return output;
                }
                else
                {
                    db.Address.Remove(dt);
                    db.SaveChanges();


                    output.Status = "OK";
                    output.Result = dt.ZipCode;
                    output.Message = "Hapus Success";
                }
                return output;
            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = "Failed Insert";
            }
            return output;
        }

        public GeneralOutputModel DeleteMasterGroup(GroupRequest pr)
        {
            try
            {
                var dt = db.GroupModel.Where(p => p.IdGroup == pr.idgrp).FirstOrDefault();
                if (dt == null)
                {
                    output.Status = "NG";
                    output.Message = "update Failed!";

                    return output;
                }
                else
                {
                    db.GroupModel.Remove(dt);
                    db.SaveChanges();


                    output.Status = "OK";
                    output.Result = dt.Name;
                    output.Message = "Hapus Success";
                }
            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = "Failed Insert";
            }
            return output;
        }

        public GeneralOutputModel DeleteContact(AddContactRequest pr)
        {
            try
            {
                var dt = db.Contacts.Where(p => p.Id == pr.Idcntc).FirstOrDefault();
                if (dt == null)
                {
                    output.Status = "NG";
                    output.Message = "update Failed!";

                    return output;
                }
                else
                {
                    db.Contacts.Remove(dt);
                    db.SaveChanges();


                    output.Status = "OK";
                    output.Result = dt.FullName;
                    output.Message = "Hapus Success";
                }
            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = "Failed Insert";
            }
            return output;
        }


        public GeneralOutputModel GetDetailContacts()
        {

            GeneralOutputModel output = new GeneralOutputModel();
            DetailContact data = new DetailContact();

            var getData = (from a in db.Contacts.OrderBy(p => p.Id)
                               join b in db.PhoneNumber on a.Id equals b.ContactId
                               join c in db.Address on a.Id equals c.ContactId
                               join d in db.ContactGroup on a.Id equals d.ContactId
                               join e in db.GroupModel on d.GroupId equals e.IdGroup
                               select new
                               {
                                   a.FullName,
                                   a.Email,
                                   b.Number,
                                   c.Street,
                                   c.City,
                                   c.State,
                                   c.ZipCode,
                                   e.Name
                               }).ToList();


            output.Status = "OK";
            output.Result = getData;
            output.Message = "Success";

            return output;
        }

        public GeneralOutputModel GetDetailPhoneNumber()
        {

            GeneralOutputModel output = new GeneralOutputModel();

            var getData = (from a in db.PhoneNumber.OrderBy(p => p.IdPhone)
                           select new
                           {
                               a.Number,
                               a.IdPhone,
                           }).ToList();


            output.Status = "OK";
            output.Result = getData;
            output.Message = "Success";

            return output;
        }

        public GeneralOutputModel GetDetailAdress()
        {

            GeneralOutputModel output = new GeneralOutputModel();

            var getData = (from a in db.Address.OrderBy(p => p.IdAdress)
                           select new
                           {
                               a.IdAdress,
                               a.ZipCode,
                               a.City,
                               a.Street,
                               a.State,
                           }).ToList();


            output.Status = "OK";
            output.Result = getData;
            output.Message = "Success";

            return output;
        }

        public GeneralOutputModel GetDetailGroup()
        {

            GeneralOutputModel output = new GeneralOutputModel();

            var getData = (from a in db.GroupModel.OrderBy(p => p.IdGroup)
                           select new
                           {
                               a.IdGroup,
                               a.Name
                           }).ToList();


            output.Status = "OK";
            output.Result = getData;
            output.Message = "Success";

            return output;
        }
    }
}

﻿using ContactApi.Data;
using ContactApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
namespace ContactApi.Controllers
{
    [ApiController]
    [Route("api/controller")]
    public class ContactsController : ControllerBase
    {
        private readonly IDataAccessProvider _dataAccessProvider;

        GeneralOutputModel output = new GeneralOutputModel();
        public ContactsController(IDataAccessProvider dataAccessProvider) 
        {
            _dataAccessProvider = dataAccessProvider;
        }

        [Route("GetDetailContacts")]
        [HttpGet]
        public IActionResult GetDetailContacts()
        {
            try
            {
                ContactModels pr = new ContactModels();

                GeneralOutputModel retrn = _dataAccessProvider.GetDetailContacts();

                if (retrn.Status == "OK")
                {
                    return Ok(retrn);
                }
                return BadRequest(retrn);
            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = ex.ToString();

                return BadRequest(output);
            }
            //return Ok(dbContext.Contacts.ToList()); 
        }

        [Route("GetDetailPhoneNumber")]
        [HttpGet]
        public IActionResult GetDetailPhoneNumber()
        {
            try
            {
                ContactModels pr = new ContactModels();

                GeneralOutputModel retrn = _dataAccessProvider.GetDetailPhoneNumber();

                if (retrn.Status == "OK")
                {
                    return Ok(retrn);
                }
                return BadRequest(retrn);
            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = ex.ToString();

                return BadRequest(output);
            }
            //return Ok(dbContext.Contacts.ToList()); 
        }

        [Route("GetDetailAdress")]
        [HttpGet]
        public IActionResult GetDetailAdress()
        {
            try
            {
                ContactModels pr = new ContactModels();

                GeneralOutputModel retrn = _dataAccessProvider.GetDetailAdress();

                if (retrn.Status == "OK")
                {
                    return Ok(retrn);
                }
                return BadRequest(retrn);
            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = ex.ToString();

                return BadRequest(output);
            }
            //return Ok(dbContext.Contacts.ToList()); 
        }

        [Route("GetDetailGroup")]
        [HttpGet]
        public IActionResult GetDetailGroup()
        {
            try
            {
                ContactModels pr = new ContactModels();

                GeneralOutputModel retrn = _dataAccessProvider.GetDetailGroup();

                if (retrn.Status == "OK")
                {
                    return Ok(retrn);
                }
                return BadRequest(retrn);
            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = ex.ToString();

                return BadRequest(output);
            }
            //return Ok(dbContext.Contacts.ToList()); 
        }

        [HttpPost]
        [Route("AddContact")]
        public IActionResult AddContact([FromForm] AddContactRequest pr)
        {
            try
            {

                GeneralOutputModel retrn = _dataAccessProvider.AddContact(pr);

                if (retrn.Status == "OK")
                {
                    return Ok(retrn);
                }
                return BadRequest(retrn);
            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = ex.ToString();

                return BadRequest(output);
            }
        }

        [HttpPost]
        [Route("AddMasterPhoneNumber")]
        public IActionResult AddMasterPhoneNumber([FromForm] PhoneNumberRequest pr)
        {
            try
            {

                GeneralOutputModel retrn = _dataAccessProvider.AddMasterPhoneNumber(pr);

                if (retrn.Status == "OK")
                {
                    return Ok(retrn);
                }
                return BadRequest(retrn);
            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = ex.ToString();

                return BadRequest(output);
            }
        }

        [HttpPost]
        [Route("AddMasterAdress")]
        public IActionResult AddMasterAdress([FromForm] AdressRequest pr)
        {
            try
            {

                GeneralOutputModel retrn = _dataAccessProvider.AddMasterAdress(pr);

                if (retrn.Status == "OK")
                {
                    return Ok(retrn);
                }
                return BadRequest(retrn);
            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = ex.ToString();

                return BadRequest(output);
            }
        }

        [HttpPost]
        [Route("AddMasterGroup")]
        public IActionResult AddMasterGroup([FromForm] GroupRequest pr)
        {
            try
            {

                GeneralOutputModel retrn = _dataAccessProvider.AddMasterGroup(pr);

                if (retrn.Status == "OK")
                {
                    return Ok(retrn);
                }
                return BadRequest(retrn);
            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = ex.ToString();

                return BadRequest(output);
            }
        }


        [HttpPost]
        [Route("UpdateContact")]
        public IActionResult UpdateContact([FromForm] AddContactRequest pr)
        {
            try
            {

                GeneralOutputModel retrn = _dataAccessProvider.AddContact(pr);

                if (retrn.Status == "OK")
                {
                    return Ok(retrn);
                }
                return BadRequest(retrn);
            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = ex.ToString();

                return BadRequest(output);
            }
        }

        [HttpPost]
        [Route("UpdateMasterPhoneNumber")]
        public IActionResult UpdateMasterPhoneNumber([FromForm] PhoneNumberRequest pr)
        {
            try
            {

                GeneralOutputModel retrn = _dataAccessProvider.UpdateMasterPhoneNumber(pr);

                if (retrn.Status == "OK")
                {
                    return Ok(retrn);
                }
                return BadRequest(retrn);
            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = ex.ToString();

                return BadRequest(output);
            }
        }

        [HttpPost]
        [Route("UpdateMasterAdress")]
        public IActionResult UpdateMasterAdress([FromForm] AdressRequest pr)
        {
            try
            {

                GeneralOutputModel retrn = _dataAccessProvider.UpdateMasterAdress(pr);

                if (retrn.Status == "OK")
                {
                    return Ok(retrn);
                }
                return BadRequest(retrn);
            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = ex.ToString();

                return BadRequest(output);
            }
        }

        [HttpPost]
        [Route("UpdateMasterGroup")]
        public IActionResult UpdateMasterGroup([FromForm] GroupRequest pr)
        {
            try
            {

                GeneralOutputModel retrn = _dataAccessProvider.UpdateMasterGroup(pr);

                if (retrn.Status == "OK")
                {
                    return Ok(retrn);
                }
                return BadRequest(retrn);
            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = ex.ToString();

                return BadRequest(output);
            }
        }

        [HttpPost]
        [Route("DeleteContact")]
        public IActionResult DeleteContact([FromForm] AddContactRequest pr)
        {
            try
            {

                GeneralOutputModel retrn = _dataAccessProvider.DeleteContact(pr);

                if (retrn.Status == "OK")
                {
                    return Ok(retrn);
                }
                return BadRequest(retrn);
            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = ex.ToString();

                return BadRequest(output);
            }
        }

        [HttpPost]
        [Route("DeleteMasterPhoneNumber")]
        public IActionResult DeleteMasterPhoneNumber([FromForm] PhoneNumberRequest pr)
        {
            try
            {

                GeneralOutputModel retrn = _dataAccessProvider.DeleteMasterPhoneNumber(pr);

                if (retrn.Status == "OK")
                {
                    return Ok(retrn);
                }
                return BadRequest(retrn);
            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = ex.ToString();

                return BadRequest(output);
            }
        }

        [HttpPost]
        [Route("DeleteMasterAdress")]
        public IActionResult DeleteMasterAdress([FromForm] AdressRequest pr)
        {
            try
            {

                GeneralOutputModel retrn = _dataAccessProvider.DeleteMasterAdress(pr);

                if (retrn.Status == "OK")
                {
                    return Ok(retrn);
                }
                return BadRequest(retrn);
            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = ex.ToString();

                return BadRequest(output);
            }
        }

        [HttpPost]
        [Route("DeleteMasterGroup")]
        public IActionResult DeleteMasterGroup([FromForm] GroupRequest pr)
        {
            try
            {

                GeneralOutputModel retrn = _dataAccessProvider.DeleteMasterGroup(pr);

                if (retrn.Status == "OK")
                {
                    return Ok(retrn);
                }
                return BadRequest(retrn);
            }
            catch (Exception ex)
            {
                output.Status = "NG";
                output.Message = ex.ToString();

                return BadRequest(output);
            }
        }


       

    }
}
